
-- SUMMARY --

Loads specific content types after page load through ajax. This is helpful if you have an aggressive page cache and
 content types containing forms, such as poll and webform together with Boost module.


-- REQUIREMENTS --

None

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70501 for further information.


-- CONFIGURATION --

* Configure user permissions in admin/people/permissions

* Configure which content types are loaded through ajax here:
  - admin/config/system/ajax_node_loader


-- CONTRIBUTORS --

Maintainer
- Andreas Radloff <https://drupal.org/user/531928>