<?php
/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function ajax_node_loader_admin_settings() {

  $types = node_type_get_names();
  $defaults = variable_get('ajax_node_loader_content_types', array());

  $form['ajax_node_loader_content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $types,
    '#default_value' => $defaults,
    '#title' => t('Choose content types to Ajax-load'),
  );

  return system_settings_form($form);
}