/**
 * This script contains ...
 */

(function ($) {
  Drupal.behaviors.ajax_node_loader = {
    attach: function (context, settings) {
      if (typeof settings.ajax_node_loader !== 'undefined') {
          var ajaxNodes = settings.ajax_node_loader;
          $('body').once('ajax_node_loader', function() {
            var ajaxSettings = {
              url: '/ajax_node_loader/nodes',
              event: 'ajax_node_loader',
              submit: {
                js : true,
                nodes: ajaxNodes
              },
              progress: {
                type: 'throbber'
              }
            };
            Drupal.ajax.ajax_node_loader = new Drupal.ajax(null, $(document.body), ajaxSettings);
            $(document.body).trigger('ajax_node_loader');
          });

      }
    }
  }
})(jQuery);